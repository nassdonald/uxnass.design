import React from "react";
import format from "date-fns/format";

export default class Jumbotron extends React.Component {
  render() {
    const {image, title, subtitle, date} = this.props;
    const formattedDate = date && format(date, "ddd, MMM D, YYYY")

    return <header className="bb bw3 b--gold cover bg-center" style={{backgroundImage: image && `url(${image})`}}>
      <div className="ph3 ph4-ns pt5 pb3 bg-black-40">
        <div className="f4-ns mw8 center white">
          <div className="mv6 tc">
            <h1 className="f1 f-subheadline-m f-headline-l lh-solid">{ title }</h1>
          { subtitle && <p className="f4 f3-ns lh-copy mt4 measure-wide center">{subtitle}</p> }
          </div>
          { formattedDate &&
            <div class="f6 cf tl">
              <div class="fl">{formattedDate}</div>
              <div class="fr white-70">
                X words - X min
              </div>
            </div>
          }
        </div>
      </div>
    </header>
  }
}
