import React from "react";

import Jumbotron from "./components/jumbotron";
import Blocks from "./components/blocks";

export default class PostPreview extends React.Component {
  render() {
    const {entry, widgetFor, getAsset} = this.props;
    let image = getAsset(entry.getIn(["data", "image"]));

    return (
      <main role="main">
        <article>
          <Jumbotron image={image} title={entry.getIn(["data", "title"])} subtitle={entry.getIn(["data", "description"])} date={entry.getIn(["data", "date"])} />
          <section class="f4-ns mv5 ph3 ph4-ns">
            <div class="cms-block cms-block-text measure-wide center lh-copy nested-copy-line-height nested-headline-line-height serif">
              <Blocks {...this.props} />
            </div>
            <div class="cms-block cms-block-text measure-wide center lh-copy nested-copy-line-height nested-headline-line-height serif">
              { widgetFor("body") }
            </div>
          </section>
        </article>
      </main>
    )
  }
}
