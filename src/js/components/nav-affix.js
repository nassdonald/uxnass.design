// Hiding fixed header - credit to Marius Craciunoiu:
// https://medium.com/@mariusc23/hide-header-on-scroll-down-show-on-scroll-up-67bbaae9a78c#.vqnj0gpcx

// Update threshold: $nav.data('nd.navaffix').threshold = 200

import $ from 'jquery'
import Util from './util'

const NavAffix = (() => {


  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  const DATA_KEY                     = 'nd.navaffix'
  const EVENT_KEY                    = `.${DATA_KEY}`
  const TRANSITION_DURATION          = 300

  const Event = {
    RESIZE            : `resize${EVENT_KEY}`,
    AFFIX             : `scroll${EVENT_KEY}`,
    FOCUS             : `focus${EVENT_KEY}`,
    BLUR              : `blur${EVENT_KEY}`
  }

  const ClassName = {
    AFFIX             : 'affix fixed',
    UNFIX             : 'absolute',
    UNFIX_INVERSE     : 'absolute white-80 inverse',
    UP                : 'up',
    FOCUS             : 'focused',
    DISABLED          : 'disabled'
  }


  /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */

  class NavAffix {

    constructor(element) {
      this._isActive            = true
      this._lastScrollTop       = 0
      this._delta               = 5
      this._element             = element
      this._navbarHeight        = $(element).outerHeight()
      this._focused             = false
      this.threshold            = this._navbarHeight // ($('#ajax-container .jumbotron').first().outerHeight() - this._navbarHeight)
      this._inverseJumbotron    = true

      $(window)
        .off(Event.AFFIX)
        .on(Event.AFFIX, () => this._didScroll = true)
        .off(Event.RESIZE)
        .on(Event.RESIZE, Util.debounce($.proxy(this._onResize, this), 200))

      this._timer = setInterval(() => {
        if (this._didScroll) {
          this._onScrolled()
          this._didScroll = false
        }
      }, 250)

      this._onScrolled()

      // Toggle nav when (focusable) child is focused/blurred
      $(this._element)
        .find('a, button, [tabindex]:not([tabindex="0"]):not([tabindex^="-"])')
        .on(Event.FOCUS, () => this._toggleNavFocus(true))
        .on(Event.BLUR, () => this._toggleNavFocus(false))
    }


    // public

    isAffixed() {
      return this._isActive && ($(window).scrollTop() > this.threshold)
    }

    toggleInverse(jumbotron) {
      if (!jumbotron) return
      this._inverseJumbotron = jumbotron.data('inverse') || false
      this._toggleAffixStyle($(window).scrollTop() > this.threshold)
    }

    toggle() {
      return this._isActive ? this.disable() : this.enable()
    }

    enable() {
      this._toggleActive(true)
    }

    disable() {
      $(this._element).addClass(ClassName.DISABLED)
      this._toggleActive(false)
    }

    dispose() {
      $(window, this._element).off(EVENT_KEY)

      clearInterval(this._timer)

      this._didScroll           = null
      this._lastScrollTop       = null
      this._delta               = null
      this._element             = null
      this._navbarHeight        = null
      this._focused             = null
      this._timer               = null
      this.threshold            = null
      this._inverseJumbotron    = null
    }


    // private

    _onScrolled() {
      var st = $(window).scrollTop()

      if (this._isActive) {
        // Make sure they scroll more than delta
        // Or return if child is focused
        if (Math.abs(this._lastScrollTop - st) <= this._delta) return

        this._toggleAffixStyle(st > this.threshold)

        // If they scrolled down and are past the navbar, add class .nav-up.
        // This is necessary so you never see what is "behind" the navbar.
        if (st <= this.threshold) $(this._element).removeClass(ClassName.UP)
        else if ((!this._isActive && st > this.threshold) ||
        (st > this._lastScrollTop && !this._focused)) {
          $(this._element).addClass(ClassName.UP)
        }
        else if (st + $(window).height() < $(document).height()) {
          $(this._element).removeClass(ClassName.UP)
        }
      }

      this._lastScrollTop = Math.max(st,0) // Prevent buggy behaviour on iOS when scrollTop < 0
    }

    _toggleNavFocus(focus) {
      this._focused = focus
      if (!this._isActive) return
      $(this._element).toggleClass(ClassName.FOCUS, focus)
    }

    _toggleAffixStyle(affix) {
      // Affix the nav with appropriate styling on scroll
      $(this._element)
        .toggleClass(ClassName.AFFIX, affix)
        .css('top', affix ? -this._navbarHeight : '')
      if (this._inverseJumbotron) {
        $(this._element)
          .removeClass(ClassName.UNFIX)
          .toggleClass(ClassName.UNFIX_INVERSE, !affix)
      } else {
        $(this._element)
          .removeClass(ClassName.UNFIX_INVERSE)
          .toggleClass(ClassName.UNFIX, !affix)
      }
    }

    _toggleActive(enable) {
      var that = this
      var $el = $(this._element)
      var transition = Util.supportsTransitionEnd()
      var affix = $(window).scrollTop() > this.threshold

      if (this._isActive === enable || (!this._isActive && !enable)) return

      this._isActive = enable

      if (enable) this._toggleAffixStyle(affix)

      function complete() {
        $el
          .toggleClass('fixed', that._isActive)
          .toggleClass('absolute', !that._isActive)

        if (!that._isActive) {
          $el.removeClass(`${ClassName.UP} ${ClassName.DISABLED}`)
          this._toggleAffixStyle(false)
        }
      }

      if (!enable) $el.removeClass(ClassName.FOCUS)

      $el
        .off(Util.TRANSITION_END)
        .toggleClass(ClassName.UP, affix)

      if (transition && !$el.hasClass('disabled')) {

        $el
          .one(Util.TRANSITION_END, $.proxy(complete, this))
          .emulateTransitionEnd(TRANSITION_DURATION)

      } else { complete.call(this) }
    }

    _onResize() {
      this._navbarHeight = $(this._element).outerHeight()
      this._toggleAffixStyle($(window).scrollTop() > this.threshold)
    }
  }

  return new NavAffix('#nav')

})(jQuery)

export let navAffix = NavAffix;
