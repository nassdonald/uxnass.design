import $ from 'jquery'
import anime from 'animejs'
import Util from './util'

const Modal = (() => {


  /**
   * ------------------------------------------------------------------------
   * Constants
   * ------------------------------------------------------------------------
   */

  const NAME                         = 'modal'
  const DATA_KEY                     = 'nd.modal'
  const EVENT_KEY                    = `.${DATA_KEY}`
  const DATA_API_KEY                 = '.data-api'
  const JQUERY_NO_CONFLICT           = $.fn[NAME]
  const TRANSITION_DURATION          = 250
  const BACKDROP_TRANSITION_DURATION = 200
  const BACKDROP_OPACITY             = .9
  const ESCAPE_KEYCODE               = 27 // KeyboardEvent.which value for Escape (Esc) key

  function fadeInOut(dialog, callback) {
    const { anim } = this
    if (anim) anim.pause()
    var prog = anim && !anim.completed && anim.began ? (anim.progress/100) : 1
    var show = this._isShown

    this.anim = anime({
      targets: dialog,
      opacity: show ? [prog < 1 ? prog : 0 , 1] : 0,
      duration: TRANSITION_DURATION * prog,
      easing: 'linear',
      complete: an => {
        $(dialog).css('opacity', '')
        if (callback) callback()
      }
    })
  }

  const Default = {
    show       : true,
    animateIn  : fadeInOut,
    animateOut : fadeInOut
  }

  const DefaultType = {
    show       : 'boolean',
    animateIn  : 'function',
    animateOut : 'function'
  }

  const Event = {
    HIDE              : `hide${EVENT_KEY}`,
    HIDDEN            : `hidden${EVENT_KEY}`,
    SHOW              : `show${EVENT_KEY}`,
    SHOWN             : `shown${EVENT_KEY}`,
    FOCUSIN           : `focusin${EVENT_KEY}`,
    CLICK_DISMISS     : `click.dismiss${EVENT_KEY}`,
    KEYDOWN_DISMISS   : `keydown.dismiss${EVENT_KEY}`,
    MOUSEUP_DISMISS   : `mouseup.dismiss${EVENT_KEY}`,
    MOUSEDOWN_DISMISS : `mousedown.dismiss${EVENT_KEY}`,
    CLICK_DATA_API    : `click${EVENT_KEY}${DATA_API_KEY}`,
    TOUCHMOVE         : `touchmove${EVENT_KEY}`
  }

  const ClassName = {
    BACKDROP           : 'modal-backdrop z-1000 fixed absolute--fill bg-gold',
    OPEN               : 'modal-open overflow-hidden'
  }

  const Selector = {
    DIALOG             : '.modal-dialog',
    DATA_TOGGLE        : '[data-toggle="modal"]',
    DATA_DISMISS       : '[data-dismiss="modal"]'
  }


  /**
   * ------------------------------------------------------------------------
   * Class Definition
   * ------------------------------------------------------------------------
   */

  class Modal {

    constructor(element, config) {
      this._config              = this._getConfig(config)
      this._element             = element
      this._dialog              = $(element).find(Selector.DIALOG)[0]
      this._backdrop            = null
      this._isShown             = false
      this._ignoreBackdropClick = false
    }


    // getters

    static get Default() {
      return Default
    }


    // public

    toggle(relatedTarget) {
      return this._isShown ? this.hide() : this.show(relatedTarget)
    }

    show(relatedTarget) {
      if (this._isTransitioning) return

      this._isTransitioning = true

      const showEvent = $.Event(Event.SHOW, { relatedTarget })

      $(this._element).trigger(showEvent)

      if (this._isShown || showEvent.isDefaultPrevented()) return

      this._isShown = true

      $('html').addClass(ClassName.OPEN)
      $(this._element).on(Event.TOUCHMOVE, e => e.stopPropagation()) // Prevent body scroll on iOS

      this._setEscapeEvent()

      $(this._element).on(
        Event.CLICK_DISMISS,
        Selector.DATA_DISMISS,
        (event) => this.hide(event)
      )

      $(this._dialog).on(Event.MOUSEDOWN_DISMISS, () => {
        $(this._element).one(Event.MOUSEUP_DISMISS, (event) => {
          if ($(event.target).is(this._element)) {
            this._ignoreBackdropClick = true
          }
        })
      })

      this._showBackdrop(() => this._showElement(relatedTarget))
    }

    hide(event) {
      if (event) event.preventDefault()

      if (this._isTransitioning || !this._isShown) return

      this._isTransitioning = true

      const hideEvent = $.Event(Event.HIDE)

      $(this._element).trigger(hideEvent)

      if (!this._isShown || hideEvent.isDefaultPrevented()) return

      this._isShown = false

      this._setEscapeEvent()

      $(document).off(Event.FOCUSIN)

      $(this._element).off(Event.CLICK_DISMISS)
      $(this._element).off(Event.TOUCHMOVE)
      $(this._dialog).off(Event.MOUSEDOWN_DISMISS)

      this._hideModal()
    }

    dispose() {
      $.removeData(this._element, DATA_KEY)

      $(window, document, this._element, this._backdrop).off(EVENT_KEY)

      this._config              = null
      this._element             = null
      this._dialog              = null
      this._backdrop            = null
      this._isShown             = null
      this._ignoreBackdropClick = null
      this._scrollbarWidth      = null
    }


    // private

    _getConfig(config) {
      config = $.extend({}, Default, config)
      Util.typeCheckConfig(NAME, config, DefaultType)
      return config
    }

    _showElement(relatedTarget) {
      if (!this._element.parentNode ||
         this._element.parentNode.nodeType !== Node.ELEMENT_NODE) {
        // don't move modals dom position
        document.body.appendChild(this._element)
      }

      // $('#mainPage').attr('aria-hidden', 'true'); // mark the main page as hidden

      this._element.style.display = 'block'
      this._element.removeAttribute('aria-hidden')
      this._element.scrollTop = 0

      Util.reflow(this._element)

      this._enforceFocus()

      const shownEvent = $.Event(Event.SHOWN, { relatedTarget })

      function complete() {
        this._element.focus()
        this._isTransitioning = false
        $(this._element).trigger(shownEvent)
      }

      if (this._config.animateIn) {
        this._config.animateIn.call(this, this._dialog, $.proxy(complete, this))
      } else { complete.call(this) }
    }

    _enforceFocus() {
      $(document)
        .off(Event.FOCUSIN) // guard against infinite focus loop
        .on(Event.FOCUSIN, (event) => {
          if (document !== event.target &&
              this._element !== event.target &&
              !$(this._element).has(event.target).length) {
            this._element.focus()
          }
        })
    }

    _setEscapeEvent() {
      if (this._isShown) {
        $(this._element).on(Event.KEYDOWN_DISMISS, (event) => {
          if (event.which === ESCAPE_KEYCODE) {
            event.preventDefault()
            this.hide()
          }
        })

      } else {
        $(this._element).off(Event.KEYDOWN_DISMISS)
      }
    }

    _hideModal() {
      function complete() {
        this._element.style.display = 'none'
        this._element.setAttribute('aria-hidden', true)
        this._isTransitioning = false
        this._showBackdrop(() => {
          $('html').removeClass(ClassName.OPEN)
          $(this._element).trigger(Event.HIDDEN)
        })
      }

      if (this._config.animateOut) {
        this._config.animateOut.call(this, this._dialog, $.proxy(complete, this))
      } else { complete.call(this) }
    }

    _removeBackdrop() {
      if (this._backdrop) {
        $(this._backdrop).remove()
        this._backdrop = null
      }
    }

    _showBackdrop(callback) {
      var show = true

      if (this._isShown && !this._backdrop) {
        this._backdrop = document.createElement('div')
        this._backdrop.className = ClassName.BACKDROP

        $(this._backdrop).appendTo(document.body)

        $(this._element).on(Event.CLICK_DISMISS, (event) => {
          if (this._ignoreBackdropClick) {
            this._ignoreBackdropClick = false
            return
          }
          if (event.target !== event.currentTarget) return
          this.hide()
        })
      } else if (!this._isShown && this._backdrop) {
        show = false
      }

      const { anim } = this._backdrop
      if (anim) anim.pause()
      var prog = anim && !anim.completed && anim.began ? (anim.progress/100) : 1

      this._backdrop.anim = anime({
        targets: this._backdrop,
        opacity: show ? [prog < BACKDROP_OPACITY ? prog : 0 , BACKDROP_OPACITY] : 0,
        duration: BACKDROP_TRANSITION_DURATION * prog,
        easing: 'linear',
        complete: an => {
          if (!show) this._removeBackdrop()
          if (callback) callback()
        }
      })
    }


    // static

    static _jQueryInterface(config, relatedTarget) {
      return this.each(function () {
        let data      = $(this).data(DATA_KEY)
        const _config = $.extend(
          {},
          Modal.Default,
          $(this).data(),
          typeof config === 'object' && config
          )

        if (!data) {
          data = new Modal(this, _config)
          $(this).data(DATA_KEY, data)
        }

        if (typeof config === 'string') {
          if (typeof data[config] === 'undefined') {
            throw new Error(`No method named "${config}"`)
          }
          data[config](relatedTarget)
        } else if (_config.show) {
          data.show(relatedTarget)
        }
      })
    }
  }


  /**
   * ------------------------------------------------------------------------
   * Data Api implementation
   * ------------------------------------------------------------------------
   */

  $(document).on(Event.CLICK_DATA_API, Selector.DATA_TOGGLE, function (event) {
    let target
    const selector = Util.getSelectorFromElement(this)

    if (selector) {
      target = $(selector)[0]
    }

    const config = $(target).data(DATA_KEY) ?
      'toggle' : $.extend({}, $(target).data(), $(this).data())

    if (this.tagName === 'A' || this.tagName === 'AREA') {
      event.preventDefault()
    }

    const $target = $(target).one(Event.SHOW, (showEvent) => {
      if (showEvent.isDefaultPrevented()) {
        // only register focus restorer if modal will actually get shown
        return
      }

      $target.one(Event.HIDDEN, () => {
        if ($(this).is(':visible')) {
          this.focus()
        }
      })
    })

    Modal._jQueryInterface.call($(target), config, this)
  })


  /**
   * ------------------------------------------------------------------------
   * jQuery
   * ------------------------------------------------------------------------
   */

  $.fn[NAME]             = Modal._jQueryInterface
  $.fn[NAME].Constructor = Modal
  $.fn[NAME].noConflict  = function () {
    $.fn[NAME] = JQUERY_NO_CONFLICT
    return Modal._jQueryInterface
  }

  return Modal

})(jQuery)

export default Modal
