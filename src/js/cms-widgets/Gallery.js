import React from "react"

const Gallery = {
  // Internal id of the component
  id: "gallery",
  // Visible label
  label: "Gallery",
  // Fields the user need to fill out when adding an instance of the component
  fields: [
    // Consider using list widget instead?
    {name: 'items', label: 'Items', widget: 'markdown', buttons: ['']}
  ],
  // Pattern to identify a block as being an instance of this component
  // pattern: /{{% ?gallery ?(?: +(?:\w+)="(?:.*?)")* ?%}}((?:.|\n)*?){{% ?\/gallery ?%}}/g,
  pattern: /{{% ?gallery ?%}}((?:.|\n)*?){{% ?\/gallery ?%}}/,
  // Function to extract data elements from the regexp match
  fromBlock: function(match) {
    var reg2 = /{{% ?gallery ?%}}((?:.|\n)*?){{% ?\/gallery ?%}}/g
    // var reg1 = /(?: +(\w+)="(.*?)")/g
    // var result;
    // var attr = {}
    // while((result = reg1.exec(match))) {
    //   attr[result[1]] = result[2]
    // }
    // return attr
    return {
      items: match[1] || reg2.exec(match[0])
    }
  },
  // Function to create a text block from an instance of this component
  toBlock: function(obj) {
    const keys = Object.keys(obj)
    const attr = keys.map((key, i) => {
      if (obj[key] && key !== "items") return ' ' + key + '="' + obj[key] + '"'
    }).join('')
    return obj.items && '{{% gallery %}}' + obj.items + '{{% /gallery %}}' || null
  },
  // Preview output for this component. Can either be a string or a React component
  // (component gives better render performance)
  toPreview: function(obj, getAsset) {
    return (
      <div className="gallery">{obj.items}</div>
    );
  }
}

export default Gallery