import React from "react"

const Figure = {
  // Internal id of the component
  id: "figure",
  // Visible label
  label: "Captioned image",
  // Fields the user need to fill out when adding an instance of the component
  fields: [
    {name: 'src', label: 'Image', widget: 'image'},
    {name: 'video', label: 'Video', widget: 'file'},
    {name: 'autoplay', label: 'Autoplay', widget: 'boolean'},
    {name: 'aspect_ratio', label: 'Aspect Ratio', widget: 'select', options: [
      {label: 'Landscape (16:9)', value: '9x16'},
      {label: 'Portrait (9:16)', value: '16x9'}
    ]},
    {name: 'alignment', label: 'Alignment', widget: 'select', default: 'center', options: [
      {label: 'Center', value: 'center'},
      {label: 'Right', value: 'right'},
      {label: 'Left', value: 'left'}
    ]},
    {name: 'link', label: 'Link URL'},
    {name: 'title', label: 'Title'},
    {name: 'alt', label: 'Alt text'},
    {name: 'caption', label: 'Caption'}
  ],
  // Pattern to identify a block as being an instance of this component
  pattern: /{{< ?figure ?(?: +(?:\w+)="(?:.*?)")+ ?>}}/,
  // Function to extract data elements from the regexp match
  fromBlock: match => {
    var reg = /(?: +(\w+)="(.*?)")/g
    var result;
    var attr = {}
    while((result = reg.exec(match))) {
      attr[result[1]] = result[2]
    }
    return match && attr
  },
  // Function to create a text block from an instance of this component
  toBlock: obj => {
    const attr = Object.keys(obj).map((key, i) => {
      if (obj[key]) return ' ' + key + '="' + obj[key] + '"'
    }).join('')
    return attr && '{{< figure' + attr + ' >}}' || null;
  },
  // Preview output for this component. Can either be a string or a React component
  // (component gives better render performance)
  toPreview: (obj, getAsset) => {
    return (
      <figure><img src={obj.src} alt={obj.alt} /><figcaption><h4>{obj.title}</h4><p>{(obj.caption || obj.alt)}</p></figcaption></figure>
    );
  }
}

export default Figure