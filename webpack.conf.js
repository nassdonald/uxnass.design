import webpack from "webpack";
import path from "path";

const webpackConfig = {
  module: {
    loaders: [
      {
        test: /\.((png)|(eot)|(woff)|(woff2)|(ttf)|(svg)|(gif)|(pdf))(\?v=\d+\.\d+\.\d+)?$/,
        loader: "file-loader?name=/[hash].[ext]"
      },
      {
        test: /\.json$/,
        loader: "json-loader"
      },
      {
        test: /\.js?$/,
        loader: "babel-loader",
        exclude: /(node_modules|dist)/,
        query: {cacheDirectory: true}
      }
    ]
  },

  plugins: [
    new webpack.ProvidePlugin({
      "fetch": "imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch"
    })
  ],

  context: path.join(__dirname, "src"),
  entry: {
    init: ["./js/init"],
    app:  ["./js/app"],
    cms:  ["./js/cms"]
  },
  output: {
    path: path.join(__dirname, "dist/js"),
    publicPath: "/",
    filename: "[name].js"
  },
  resolve: {
    alias: {
      "util": path.resolve("src/js/components", "util"),
      "PhotoSwipeUI_Default": "photoswipe/src/js/ui/photoswipe-ui-default"
    }
  },
  externals: {
    jquery: "jQuery"
  }
}

if (process.env.NODE_ENV === "production") {
  webpackConfig.plugins.push(new webpack.optimize.UglifyJsPlugin({
    mangle: {
      except: ["$super", "$", "exports", "require"]
    }
  }));
}
else {
  webpackConfig.devtool = "cheap-module-source-map";
}

export default webpackConfig;
