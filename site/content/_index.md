---
title: |
  Designer <span class="gold">for hire</span>
intro: |
  Need a product designer to take your digital experiences to the next level? <a href="#intro" class="no-underline gold o-80 glow">You're in luck!</a>
bg_image: /img/devices-bg-black.jpg
bg_color: bg-near-black
introduction:
  heading: Hi!
  text: |
    I’m Nass, multi-disciplined designer of beautifully functional, scalable, user-friendly interfaces, and engaging user experiences.

    Having developed a breadth of knowledge and skills over the years, from making coffee to writing JavaScript, I’m always ready for a new creative challenge.
past_clients:
  heading: Clients
  text: For the past 9 years, I have been fortunate to have produced work for a broad range of brands; startups to enterprises.
  sprite: img/clients-logos-sprite@2x.png
  clients:
    - name: Airparks
      id: airparks
    - name: Alton Towers
      id: alton-towers
    - name: BBC Worldwide
      id: bbc-worldwide
    - name: BCP
      id: bcp
    - name: easyJet
      id: easyjet
    - name: Essential Travel
      id: essential-travel
    - name: Harry Potter and the Cursed Child - parts one and two
      id: harry-potter
    - name: Holiday Extras
      id: holiday-extras
    - name: JetParks.co.uk
      id: jetparks
    - name: Legoland Windsor
      id: legoland
    - name: Merlin Entertainments Group
      id: merlin
    - name: National Express
      id: national-express
    - name: Picturehouse
      id: picturehouse
    - name: Purple Parking
      id: purple-parking
    - name: SalesSeek
      id: salesseek
    - name: Thomas Cook
      id: thomas-cook
projects:
  title: Projects
  text: |
    Check out a few of my favourite projects, then [get in touch](/contact/).
---
