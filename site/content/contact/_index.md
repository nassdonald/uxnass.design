---
title: Let's talk
description: If you need an experienced designer for your next exciting project, get in touch.
email_link: mailto:nassimdonald@gmail.com
email_text: >-
  nassimdonald<code>@</code>gmail.com
phone_link: tel:+447824812697
phone_text: (+44) 7824 812697
location_text: >-
  Currently, I’m based in the beautiful historic town of Canterbury, Kent (UK). I am open to remote working, and relocation opportunities.
---
