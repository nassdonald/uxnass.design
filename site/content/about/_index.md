---
title: "Design experience"
description: All about my background, skills, experience and more.
intro: >-
  Having studied and worked in the field for over a decade, it's safe to say design is one of my greatest passions in life.


  Alongside honing my design skills through various mediums, I've developed a knack for coding too. The combination of these skills has proved invaluable in the realms of UX, UI and product design.
keywords: ["suggested"]
date: 2018-07-07T13:07:19-05:00
project_icon_large: /img/nass-app-icon-large@2x.png
approach:
  heading: Approach
  subheading: My mantras for maintaining a healthy career in design
  mantras:
  - svg: search-plus
    heading: Attention to detail
    text: Balance big-picture thinking with sweating the details. Where there is time, there’s always room for improvement.
  - svg: users
    heading: Collaboration
    text: Recognise the value and importance of giving and receiving feedback. Design should be a collaborative process and a shared responsibility.
  - svg: comments-bubble
    heading: Communication
    text: Ensure a closed loop of feedback for more meaningful iterations. Empathy and listening is at the centre of good design.
  - svg: info-circle
    heading: Learning
    text: Embrace the learning experience and share knowledge with others. Welcome new challenges, feed curiosities and remain open to all possibilities in the creative process.
  - svg: heart
    heading: Passion
    text: Demonstrate a passion for the craft. Design for the love of design and  Rejoice in the success of any project - even learning from failure is a success.
  - svg: leaf
    heading: Simplifying complexity
    text: Develop a capacity for tackling complex problems, creating simple solutions, and bringing clarity, order and understanding.
  - svg: list
    heading: Systematic thinking
    text: Understand how all the pieces of the puzzle fit together to create a unified experience. A high-level perspective of the wider context yields more cohesive design.
  - svg: thumbs-up
    heading: User-focus
    text: Be a passionate advocate of a user-centred approach to design, putting the user at the heart of every endeavour. Take pride in removing friction at every point of the user journey.
skills_and_knowledge:
  description: >-
    They say a good workman never blames his tools. I believe that a good workman always uses good tools.


    My skillset and knowledge have expanded rapidly with each new project, including and not limited to the following:
  footnote: >-
    **Fun fact:** I built this site with Hugo, Netlify CMS, Tachyons (SCSS), Anime, Gulp and Webpack.
  heading: Skills & knowledge
  skills:
    - "User personas and user stories"
    - "Sitemaps and user flows"
    - "Design systems"
    - "Pattern libraries"
    - "Brand and style guides"
    - "Responsive design"
    - "Adaptive design"
    - "User and usability testing"
    - "User-centric design"
    - "A/B testing"
    - "Data-driven design"
    - "Compatibility testing"
    - "A11Y & tota11y"
    - "Low/high fidelity prototyping"
    - "Figma / Sketch / Framer X"
    - "Adobe CS"
    - "InVision / Origami"
    - "CSS / LESS / SCSS"
    - "Tachyons / Basscss / funcssion"
    - "Bootstrap / Semantic UI / UIkit"
    - "JS and jQuery etc."
    - "GSAP / Velocity / Anime"
    - "D3 / Chart JS"
    - "React (still learning)"
    - "Backbone & Marionette"
    - "Underscore"
    - "Handlebars"
    - "Hugo"
    - "Wordpress / Squarespace"
    - "Gulp & Webpack / Require JS"
    - "Netlify CMS"
    - "Git: Github / Gitlab / Bitbucket"
    - "JIRA: Agile & Scrum"
professional_background:
  heading: Background
  timeline:
    - content: >-
        Leading the initiative to create a company-wide Design System. The primary objective is to unite the design and engineering teams through a 'single source of truth' for components, styles and design patterns. This role has required mediating between the worlds of design and development and putting my experience in both areas to best use.


        As a senior product designer, amongst a team of 5 designers, my responsibilities also cover a large part of product development. In order to guide the evolution of the product to meet business objectives in short timeframes, I have introduced some powerful new tools and processes to the team to help us design faster, at a higher standard, and with a heavier emphasis on user-centric design.


        My latest work has been largely research driven, including direct user interviews with clients and target customers, and analysing data from FullStory and Mixpanel. These efforts have resulted in a much clearer vision of the product's future, focusing our feature development on validated hypotheses and real use cases.
      dates: 2018 - Present
      role: Senior Product Designer
      summary: Innovative London-based back-office, VMS, and payroll software startup.
      title: Engage
    - achievements:
        - >-
          **TTG Awards** - Car Hire and Travel Essentials Company of the Year.
        - >-
          **Travel Weekly Globe Awards** - Best Travel Insurance Provider (8 years running).
      content: >-
        Worked alongside one other designer, providing UX/UI support to the entire business, including a web team of over fifty developers.


        Built an extensive [pattern library](/article/pattern-library) and theming system, providing the framework for building UIs across an enormous collection of sites, with powerful over-branding functionality for internal brands and external clients such as easyJet, British Airways, Thomas Cook, TUI, Money Supermarket, National Express, Saga, and Merlin (Alton Towers, Legoland, Thorpe Park etc.).


        Day-to-day included wireframing, prototyping, user-testing, and iteratively designing end-to-end user flows – from landing pages and booking flows to emails and account management platforms – for a variety of products such as airport parking, hotels, package holidays, travel insurance and currency cards.
      dates: 2016 - 2018
      link: /project/holiday-extras
      role: Senior UX/UI Designer
      summary: International travel company and UK market leader for travel extras.
      title: Holiday Extras
    - achievements:
        - >-
          Licensed by **BBC Worldwide**
      content: >-
        Designed and developed an end-to-end e-commerce and product website solution for a brand new educational children’s programme.


        In a restrictive time frame and budget, an ambitious MVP was delivered, including landing pages, branded graphics and logo assets, bespoke interactive animations, and a multi-currency booking flow.
      dates: 2015
      role: Designer / Developer
      summary: Website and store for cartoon-based teaching programme.
      title: Phonics School
    - achievements:
        - >-
          Partnered with **Sage**, **Experian**, and **Virgin Startup**
      content: >-
        Managed and mentored a team of three designers, responsible for all aspects of design, from UX/UI of mobile and web apps, to product website, branding and marketing materials.


        Built foundations for web app and website architecture, and oversaw and actively contributed to their development thereafter. Provided training to front-end devs in JS, Backbone and Marionette.


        The product consisted of several innovative and bespoke features including complex interactive data visualisations, web tracking, multi-currency sales forecasting, task management, automation, CRM, data import and mapping, and multiple dashboards for various user roles.
      dates: 2012 - 2015
      link: /project/salesseek
      role: Lead Designer / F.E.D.
      summary: Fast-paced, London-based, sales and marketing software startup.
      title: SalesSeek
    - content: >-
        Led by data visualisation experts Adam Frost and Tobias Sturt, the course explored the world of data visualisation in detail.


        Skills developed included data discovery and analysis, memorable visual storytelling, graph and chart design, static (infographics) and interactive visualisation, and much more.
      dates: 2013
      role: The Guardian
      title: Data Visualisation
    - achievements:
        - >-
          Received **John Gray Award** (2010) for student excellence in 3rd year.
      content: >-
        In addition to core curriculum, the multi-disciplinary course included various modules such as:


        * Management of Innovation

        * Consumer Behaviour

        * Design for Manufacture

        * Marketing

        * Business Startup
      dates: 2007 - 2011
      role: Gray’s School of Art
      title: (BDES) Product Design
    - achievements:
        - >-
          **Alford Academy’s Worst Haircut** - 1st prize (1999-2000)
      content: >-
        As far back as I can remember, I’ve always had a passion for creativity. Before I was old enough for computer keyboards, it was all about the musical keyboards. It must have started from the cot in the basement of my father’s music shop, with the gentle finger-picking of Stairway to Heaven from above lulling me to sleep.


        Fast-forward nearly 3 decades... when I’m not crafting highly-detailed visual interfaces and intuitive user experiences, you’ll find me playing in a band (<a href="https://okbuttonmusic.com" target="_blank">OK Button</a>) with my super talented singer-songwriter wife, messing around with my camera, longboarding, or sampling the finest coffee in town – sometimes all at once.
      dates: 1988
      role: Scotland
      title: Born in Aberdeen
---

# Location
Currently, I’m based in Canterbury, Kent (UK). I am open to fully or partly remote working opportunities in any industry.
